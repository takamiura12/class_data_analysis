---
marp: true
theme: lecture
math: mathjax
title: データ分析I_1
paginate: true
--- 

## データ分析 I 第 1 回
### 三浦貴弘
### 2024/06/17

---
<!-- _class: large_text -->
<!-- _header: この講義の目的 -->

- Python の基礎的な使い方、演習を通じて
  - 基礎的なデータ分析のスキルを身につける
  - データ分析に必要な周辺知識などを学ぶ

---
<!-- _header: どのようにデータ分析を行うのか? -->
<!-- _class: sm_bottom_img -->
<!-- _footer: "![](../../assets/figures/analysis_flow.png)" -->

- 分析コードを書くのがプログラミング?
  - なんかすごい技術が必要?

---
<!-- _header: 実際のデータ分析で起こる過程 -->

- データは基本的にない
- データはあっても、汚い
  - 作業者のミス、変な値、異常値、etc
- データが汚くなくても、分析用に整っていない
  - 変数の作成、データの結合・圧縮、etc
- データ分析に則しているかチェックが必要
  - 記述統計、グラフ、テスト、etc

* データ分析までが遠い!

---
<!-- _header: 機械学習の全体のプロセスイメージ -->
<!-- _class: sm_bottom_img -->
<!-- _footer: "![](../../assets/paper/Sculley_2015_ML.png)" -->

---
<!-- _header: データ分析に必要なことは何か -->

- データ分析では、多くの作業が発生する
  - データ収集
  - データの確認
  - 変数作成
  - グラフ作成
  - 分析の実装
  - 他にもたくさん
* ほとんどの作業は **手続き的な知識** (こうしたらこうなる) を要する
  - ある程度 AI で代替可能
* 効率的に作業を達成するには **計画** (何をすればいいか) が必要
  - 判断は自分でする必要

---
<!-- _header: 授業計画 -->

- 全体の理解度によって授業計画を若干修正する可能性

- 講義 1 ~ 4 回目
  - 基本的な文法・考え方について学びます
  - 四則演算から関数まで
- 講義 5 回目: 演習
  - 1 ~ 4 回で学んだことを活用した軽いデータ分析
- 講義 6 ~ 7 回目
  - モジュールやクラスなど、発展的なトピックについて学びます
- 講義 8 回目
  - 演習予定

---
<!-- _header: 成績評価 -->
<!-- _class: left_img -->
<!-- _header: 成績評価 -->
- 小テスト: 10%
  - 授業開始時にやります
- 課題: 30%
- 最終課題: 60%

* 出席点はありません !

<!-- min = 3 -->
---

<!-- _header: 連絡先・オフィスアワーなど -->

- オフィスアワー
  - 西 3 号館 504 号室 (三浦研究室)
  - 月曜 3 限 (13:10 ~ 14:40)
    - 昼対応可 (事前に連絡をお願いします)
  - メール、Teams、Moodle でも対応可能

- 連絡先
  - takam@wakayama-u.ac.jp

---
<!-- _header: Python とは? -->
<!-- _class: right_thin_img -->
<!-- _footer: "![](../../assets/figures/python-logo-only.png)" -->

- プログラミング言語のひとつ
- ABC という学習用プログラムからヒントを得て開発された
- 開発者: Guido van Rossum (グイド・ヴァンロッサム)
  - 2018 年に Python の仕様策定の立場を引退
- 名前の元ネタは 「空飛ぶモンティ・パイソン」
  - 1969 ~ 1974 年にイギリスで放映されたコメディ番組

---
<!-- _header: なぜ Python? -->
<!-- _class: right_thin_img -->
<!-- _footer: "![](../../assets/figures/programming_ranking.png)" -->

- 初心者にも分かりやすい
- 幅広い用途
  - Web 開発・データ分析など
- 豊富な勉強環境
  - 本、Web サイト、動画
- 人気
  - 大学外でも使える機会が多い

---
<!-- _header: Python のデメリット -->

- Fortran や C などと比べて遅い
- しかし、
  - numpy や pandas などは C をベースに実装されている
  - numba などの高速化用ライブラリがある
  - 3.11 へのバージョン変更で速度が向上した (最大 10 ~ 60%)
- ただし、基本的には Python で事足りる

---
<!-- _header: R との比較 -->
- R: 科学計算用のプログラミング言語

- 基本的な操作はどちらもできる
  - pandas (Python) ≈ dplyr (R)
- 図の作成は R の方が楽
  - Python だと matplotlib だが、かなり煩雑
  - R だと ggplot2
    - Plotnine というライブラリが Python にもあるが、一部機能不足
- 最新の統計手法は R で実装されている
  - Python は機械学習に強い
